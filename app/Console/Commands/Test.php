<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.10.18
 * Time: 17:09
 */

namespace App\Console\Commands;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;


class Test extends Command
{
    const PATH_FOR_API = "http://192.168.1.3:18281/json_rpc";

    const
        DIFFICULTY_CUT = 60,
        DIFFICULTY_WINDOW = 720,
        DIFFICULTY_BLOCKS_COUNT = 735;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test command';

    protected $client;

    protected $blocks;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = new Client();
        $this->blocks = [];
    }

    public function handle()
    {
        $info = $this->getInfo();
        $height = $info['height'] - 1;
        $cumulativeDiff = $info['cumulative_difficulty'];

        $this->blocks = $this->getBlockByRange($height - self::DIFFICULTY_BLOCKS_COUNT + 1, $height);
        $cumulativeDiff += $this->blocks[$height]['difficulty'];

        $this->alert("Forecast difficulty");
        for ($i = 1; $i <= 75; $i++) {
            $newDifficulty = $this->getNextDifficulty($height, $cumulativeDiff);
            $height++;
            $cumulativeDiff += $newDifficulty;
            $this->blocks[$height] = [
                'difficulty' => $newDifficulty,
                'timestamp' => 0
            ];
            $this->comment("$i) blockId = $height; diff = $newDifficulty");
        }
    }
    
    /**
     * @param $lastHeight
     * @param $cumulativeDiff
     * @return int
     * @throws \Exception
     */
    public function getNextDifficulty($lastHeight, $cumulativeDiff) {
        $targetSeconds = 60;
        $timestamps = [];
        $difficulties = [];

        $heightIndex = $lastHeight;
        $counter = self::DIFFICULTY_BLOCKS_COUNT-2;
        while($heightIndex > ($lastHeight - self::DIFFICULTY_BLOCKS_COUNT)) {
            $block = $this->blocks[$heightIndex];

            if($heightIndex != $lastHeight) {
                $cumulativeDiff -= $block['difficulty'];
            }
            $difficulties[$counter] = $cumulativeDiff;
            $timestamps[$counter] = $block['timestamp'];

            $heightIndex--;
            $counter--;
        }

        if(count($timestamps) > self::DIFFICULTY_WINDOW)
        {
            $timestamps = array_slice($timestamps, 0, self::DIFFICULTY_WINDOW, true);
            $difficulties = array_slice($difficulties, 0, self::DIFFICULTY_WINDOW, true);
        }

        /** search interval index  */
        $length = count($timestamps);
        if(!($length <= self::DIFFICULTY_WINDOW)) {
            throw new \Exception("length ($length) <= DIFFICULTY_WINDOW ");
        }
        if ($length <= self::DIFFICULTY_WINDOW - 2 * self::DIFFICULTY_CUT) {
            $cut_begin = 0;
            $cut_end = $length;
        } else {
            $cut_begin = ($length - (self::DIFFICULTY_WINDOW - 2 * self::DIFFICULTY_CUT) + 1) / 2;
            $cut_end = $cut_begin + (self::DIFFICULTY_WINDOW - 2 * self::DIFFICULTY_CUT);
        }
        if(!($cut_begin + 2 <= $cut_end && $cut_end <= $length)) {
            throw new \Exception("cut_begin = $cut_begin && cut_end = $cut_end");
        }

        /** search $time_span */
        $time_span = $timestamps[$cut_end - 1] - $timestamps[$cut_begin];
        if ($time_span == 0) {
            $time_span = 1;
        }

        /** search $total_work */
        $total_work = $difficulties[$cut_end - 1] - $difficulties[$cut_begin];
        if(!($total_work > 0)) {
            throw new \Exception("total_work = $total_work");
        }

        /**  function mul() */
        $low = $total_work * $targetSeconds;

        return (int)(($low + $time_span - 1) / $time_span);
    }
    
    /**
     * @param int $start
     * @param int $end
     * @return array|bool
     */
    public function getBlockByRange(int $start, int $end) {
        $response = $this->sendRequest([
            "jsonrpc" => "2.0",
            "id" => "0",
            "method" => "get_block_headers_range",
            "params" => [
                "start_height" => $start,
                "end_height" => $end
            ]
        ]);
        if(!empty($response)) {
            $item = json_decode($response, true);
            $headers = $item['result']['headers'] ?? null;
            if($headers) {
                $result = [];
                foreach ($headers as $item) {
                    $result[$item['height']] = $item;
                }
                return $result;
            }
        }
        return false;

    }
    
    /**
     * @return bool|null
     */
    public function getInfo(){
        $response = $this->sendRequest([
            "jsonrpc" => "2.0",
            "id" => "0",
            "method" => "get_info",
        ]);
        if(!empty($response)) {
            $item = json_decode($response, true);
            $val = $item['result'] ?? null;
            if($val) {
                return $val;
            }
        }
        return false;
    }
    
    /**
     * @param array $body
     * @return null|string
     */
    public function sendRequest(array $body) {
        try {
            $res = $this->client->request(
                'POST',
                self::PATH_FOR_API,
                [
                    RequestOptions::JSON => $body
                ]
            );
            if($res->getStatusCode() == 200) {
                return (string)$res->getBody();
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            $this->alert($e->getTraceAsString());
        }
        return null;
    }
}